# hexo-theme-inside-ng

hexo-theme-inside front-end project, which is part of [hexo-theme-inside] project. Built with the lastest version of [Angular].

## Build

`node build`

## SSR build (Server-side render support)

`npm run build:ssr`

Go check out my blog (https://blog.oniuo.com) for more information.

[hexo-theme-inside]: https://github.com/ikeq/hexo-theme-inside
[Angular]: https://angular.io
