const fs = require('fs-extra');
const exec = require('child_process').exec;
const path = require('path');
const Listr = require('listr');
const utils = require('./build/utils');

const root = path.join(__dirname, 'dist');
const src = path.join(root, 'src');
const dist = path.join(root, 'browser');
const tmp = path.join(root, 'tmp');

module.exports = function (options = {}) {
  const tasks = [
    {
      title: 'clean',
      task: () => fs.remove(root)
    },
    {
      title: 'build theme',
      task: () => cmd('npm run build:theme')
    },
    {
      title: 'build styles',
      task: () => cmd('npm run build:styles')
    },
    {
      title: 'build ng',
      task: () => cmd('npm run build:ng')
    },
    {
      title: 'build ng:ssr',
      task: () => cmd('npm run build:ssr')
    },
    {
      title: 'finish',
      task: () => {
        // copy _theme.js into dist/browser
        fs.copySync(path.join(tmp, 'browser'), dist);

        // get css
        const css = fs.readFileSync(path.join(tmp, '../../public/styles.css'))
        fs.writeFileSync(dist + `/styles.${utils.md5(css, 20)}.css`, css);

        return Promise.resolve()
          // .then(() => fs.remove(path.join(dist, 'api')))
          // .then(() => fs.remove(path.join(dist, 'favicon.ico')))
          .then(() => fs.remove(path.join(dist, 'index.html')))
          .then(() => {
            const dir = fs.readdirSync(dist);
            const componentMap = require(path.join(tmp, './components.json'));
            const cssMap = require(path.join(tmp, './css'));
            const resources = {
              root: componentMap['is-root'],
              styles: [],
              scripts: [],
              class: Object.keys(cssMap)
                .filter(i => i.startsWith('φinject__'))
                .reduce((map, current) => {
                  map[current.substring(9)] = cssMap[current]
                  return map
                }, {})
            };
            const styleOrder = ['styles'];
            const scriptOrder = ['runtime', 'polyfills', 'main'];

            dir.forEach(i => {
              // [name]         .[hash:20].js
              // [name]-[target].[hash:20].js
              // [name]         .[hash:20].css
              const [prefix, hash, lang] = i.split('.')
              const [name, target] = prefix.split('-')
              const filePath = path.join(dist, i)

              if (~['es5'].indexOf(target)) return fs.remove(filePath)

              if (name === 'theme') {
                resources.theme = i;
                return;
              }

              if (lang === 'css') return resources.styles.push(i)
              if (lang === 'js') return resources.scripts.push(i)
            });

            resources.styles = resources.styles.sort(rSort(styleOrder));
            resources.scripts = resources.scripts.sort(rSort(scriptOrder));

            return fs.writeJSON(path.join(dist, '_manifest.json'), resources);

            function rSort(orders) {
              return (a, b) => {
                const ai = orders.indexOf(getName(a));
                const bi = orders.indexOf(getName(b));

                if (ai === bi) return 0;
                if (ai === -1) return 1;
                if (bi === -1) return -1;
                return ai - bi;
              }
            }

            function getName(vTag) {
              return (typeof vTag === 'string' ? vTag : (vTag.src || vTag.href)).split(/[\.-]/)[0]
            }
          })
          .then(() => fs.remove(tmp))
          .then(() => fs.remove(src));
      }
    }
  ];

  return new Listr(tasks);
};

function cmd(c) {
  return new Promise((resolve, reject) => {
    exec(c, { cwd: __dirname }, err => {
      if (err) reject(err);

      resolve();
    })
  })
}
