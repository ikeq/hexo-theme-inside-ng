export const g: {
  win: any;
  doc: any;
  loc: any;
  isServer?: true;
} = {
  win: window,
  doc: document,
  loc: location
};
