import { DOCUMENT } from '@angular/common';
import { Component, ElementRef, HostListener, Inject, OnInit, ViewChild } from '@angular/core';
import { Meta, Title } from '@angular/platform-browser';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { filter } from 'rxjs/operators';
import { Config, ScreenType } from '../../interface';
import { AppService, DataResolver, DeviceService, ScrollState } from '../../services';
import { animate, getRGBA, touchable, transitionEnd, xProperty } from '../../utils';
import { FabAction } from '../fab/fab.component';
import { SidebarComponent } from '../sidebar/sidebar.component';
import { TocComponent } from '../toc/toc.component';
import { VPageComponent } from '../v-page/v-page.component';
import { VPostComponent } from '../v-post/v-post.component';
import { VPostListComponent } from '../v-post-list/v-post-list.component';
import { g } from '../../global';

const originUrlLength = g.loc.origin.length + 1;

@Component({
  selector: 'is-root',
  templateUrl: './app.component.html',
  host: { class: 'φapp' },
})
export class AppComponent implements OnInit {
  private pageHeight: number;
  // private pageWidth: number;
  private sidebarWidth: number;
  private indexScrollTop: number;
  private transformer: (translate?: number, scale?: number) => string;
  private isTransiting: boolean; // indicate if <main> is in a transition state
  private themeColor: string;
  private screenType: ScreenType;

  @ViewChild(TocComponent) private toc: TocComponent;
  @ViewChild(SidebarComponent, { static: true }) private sidebar: SidebarComponent;
  @ViewChild('outer', { static: true }) private outerRef: ElementRef;
  @ViewChild('page', { static: true }) private pageRef: ElementRef;
  tocData;
  isTocOpen = false;

  config: Config;
  isSidebarOpen = false;
  isTouchable = false;
  currentId: string;
  scrollPercent = 0;
  scrollTop = 0;

  isSearchOpen = false;

  constructor(
    private app: AppService,
    private route: ActivatedRoute,
    private router: Router,
    private meta: Meta,
    private title: Title,
    private device: DeviceService,
    private er: ElementRef,
    @Inject(DOCUMENT) private doc: Document
  ) {
    this.config = app.config;

    // update routes
    const pageRoutes = (this.config.routes.pages || []).map(path => {
      return { path, component: VPageComponent, resolve: { page: DataResolver }, data: { id: 'page' } };
    });
    const postRoutes = this.config.routes.posts.map(path => {
      return { path, component: VPostComponent, resolve: { post: DataResolver }, data: { id: 'post' } };
    });

    let blackList = [];
    if (this.config.count.tags < 1) blackList = ['tag', 'tags'];
    if (this.config.count.categories < 1) blackList.push('categories');
    if (!this.config.search || !this.config.search.page) blackList.push('search');
    if (blackList.length) this.router.config = this.router.config.filter(r => !r.data || blackList.indexOf(r.data.id) === -1);

    // built-in routes, page routes, post routes, *
    const routes = this.router.config.concat(
      pageRoutes,
      postRoutes,
      { path: '**', redirectTo: '404' }
    );

    // if index position is not used, fill it with `page/1`
    if (!routes.find(i => i.path === '')) {
      routes.unshift({
        path: '',
        component: VPostListComponent,
        resolve: { postList: DataResolver },
        data: { id: 'home' },
      });
    }

    this.router.resetConfig(routes);
  }

  ngOnInit() {
    this.app.root = this.er.nativeElement;
    // subscribe window resize
    this.device.media.subscribe(screen => {
      this.screenType = screen.type;

      // refresh
      this.pageHeight = this.pageRef.nativeElement.offsetHeight;
      // this.pageWidth = this.pageRef.nativeElement.offsetWidth;
      this.sidebarWidth = this.sidebar.innerRef.nativeElement.offsetWidth;
      this.device.refreshScroll();

      if (screen.type === ScreenType.sm) {
        this.transformer = (t, s) => `translate3d(${t}px, 0, 0) scale3d(${s},${s},1)`;
      }
      else if (screen.type === ScreenType.md) {
        this.transformer = (t) => `translate3d(${t}px, 0, 0)`;
      }
      else {
        this.transformer = () => '';
      }

      if (screen.type === ScreenType.lg) {
        if (this.isSidebarOpen) {
          this.toggleSb(true);
        }
        this.sidebar.er.nativeElement.style.opacity = '';
      } else {
        if (this.transformer && this.isSidebarOpen) this.toggleSb(true);
        if (!this.isTouchable) this.initTouch();
      }
    });

    // subscribe window scroll
    this.device.initScroll(this.pageRef.nativeElement);
    this.device.scroll.subscribe(({ scrollTop }: ScrollState) => {
      this.scrollTop = scrollTop;
      if (this.currentId === 'post' || this.currentId === 'page') {
        let scale = scrollTop / (this.pageRef.nativeElement.scrollHeight - this.pageHeight);
        if (scale < 0) scale = 0;
        if (scale > 1) scale = 1;
        this.scrollPercent = Math.round(scale * 100);
      }
      else
        // reset
        this.scrollPercent = 0;

      if (this.isTocOpen && this.tocData && this.toc) this.toc.syncPosition();

      // store scroll position
      if (this.currentId === 'home') this.indexScrollTop = scrollTop;
    });

    // subscribe route change
    this.router.events.pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(() => {
        // close sidebar when route is changed
        if (this.screenType !== ScreenType.lg && this.isSidebarOpen) this.toggleSb();
        if (this.isTocOpen) this.toggleToc();

        this.device.refreshScroll();

        const data = this.route.snapshot.children[0].children.length ?
          this.route.snapshot.children[0].children[0].data :
          this.route.snapshot.children[0].data;

        this.currentId = data.id;

        if (this.currentId === 'home') {
          setTimeout(() => {
            this.pageRef.nativeElement.scrollTop = this.indexScrollTop || 0;
          }, 0);
        } else if (this.currentId === 'post' || this.currentId === 'page') {
          const fragment = this.route.fragment['value'];
          if (fragment) {
            setTimeout(() => {
              const heading = this.doc.getElementById(fragment);
              if (heading) {
                heading.scrollIntoView(true);
              }
            }, 24);
          } else {
            this.pageRef.nativeElement.scrollTop = 0;
          }
          // append link to anchor elements
          setTimeout(() => {
            const { link } = data[this.currentId];
            const anchors = this.pageRef.nativeElement.querySelectorAll('.φv-post__body a,.φv-page__body a');
            Array.from(anchors).forEach((a: HTMLAnchorElement) => {
              if (a.href[originUrlLength] === '#') a.href = link + a.href.substring(originUrlLength);
            });
          });
        } else {
          this.pageRef.nativeElement.scrollTop = 0;
        }

        // toc support
        if (data[this.currentId] && data[this.currentId].toc) {
          this.tocData = data[this.currentId].toc;
        } else {
          this.tocData = null;
        }

        this.setTitle(data);

        if (data.post && data.post.thumbnail) {
          if (data.post.color) this.themeColor = this.setColor(data.post.color);
          else if (data.post.thumbnail)
            getRGBA(data.post.thumbnail)
              .then(color => this.themeColor = this.setColor(color))
              .catch(() => this.setColor());
        } else this.themeColor = this.setColor();
      });
  }

  private initTouch() {
    this.isTouchable = true;
    const mainTouchable = touchable(this.pageRef.nativeElement, { scale: .382 });

    mainTouchable.subscribe(touch => {
      const threshold = this.device.width * .06,
        maxWidth = this.sidebarWidth,
        close = this.isSidebarOpen;

      let offset = touch.offset, duration;

      // Return if sidebar is closed
      if (!this.isSidebarOpen) return;

      // At least 1px to trigger transition
      if (offset > maxWidth - 1) offset = maxWidth - 1;
      if (offset < 1) offset = 1;

      duration = ~~(maxWidth - offset);

      if (!touch.isEnd) {
        this.stepSb(close ? (maxWidth - offset) : offset, 0);
      } else {
        // Less then threshold, reverse action
        if (offset < threshold) this.stepSb(close ? maxWidth : 0, threshold * 5);
        else {
          this.stepSb(close ? 0 : maxWidth, duration);
          this.isSidebarOpen = !this.isSidebarOpen;
        }
      }
    });
    mainTouchable.subscribe(touch => {
      if (!this.tocData) return;

      const threshold = this.device.width * .04,
        maxWidth = this.toc.width,
        close = this.isTocOpen;

      let offset = touch.offset, duration;

      // Return if toc is closed
      // Return if it is transiting
      // Return if sidebar is open
      if (!this.isTocOpen || this.isTransiting || this.isSidebarOpen) return;

      // At least 1px to trigger transition
      if (offset > maxWidth - 1) offset = maxWidth - 1;
      if (offset < 1) offset = 1;

      duration = ~~(maxWidth - offset);

      if (!touch.isEnd) {
        this.toc.step(close ? offset : (maxWidth - offset), 0);
      } else {
        // Less then threshold, reverse action
        if (offset < threshold) this.toc.step(close ? 0 : maxWidth, threshold * 5);
        else {
          this.toc.step(close ? maxWidth : 0, duration);
          this.isTocOpen = !this.isTocOpen;

          // sync toc position
          if (!close) this.toc.syncPosition();
        }
      }
    });
  }

  stepSb(translate: number, duration: number) {
    const outer = this.outerRef.nativeElement;
    const sidebar = this.sidebar.er.nativeElement;
    // `this.app.config` is a fresh extend of `window.__inside__`;
    const colorWhenSiding = this.app.config.color[0] || this.config.color[0];

    if (duration > 0) {
      this.isTransiting = true;
      if (this.isTocOpen) this.toggleToc();
      outer.classList.add('φapp__outer--pushed');
      // set theme-color to match background
      this.setColor(colorWhenSiding);

      transitionEnd(outer, () => {
        if (translate === 0) {
          outer.classList.remove('φapp__outer--pushed');
          this.setColor(this.themeColor);
        }

        outer.style[xProperty('transitionDuration')] = outer.style[xProperty('transitionProperty')] = '';
        sidebar.style[xProperty('transitionDuration')] = sidebar.style[xProperty('transitionProperty')] = '';
        this.isTransiting = false;
      });
    }
    // touchmove
    // Make sure the following actions will only do once
    else if (!this.isTransiting) {
      // This is safe, because once a swipe action is happened, it will end up with a transition
      this.isTransiting = true;

      outer.classList.add('φapp__outer--pushed');
      // set theme-color to match background
      this.setColor(colorWhenSiding);
    }

    outer.style[xProperty('transitionProperty')] = xProperty('transform', true) + ',border-radius';
    outer.style[xProperty('transform')] = this.transformer(translate, 1 - .14 * translate / this.sidebarWidth);
    outer.style[xProperty('transitionDuration')] = `${duration}ms`;

    sidebar.style.opacity = translate / this.sidebarWidth;
    sidebar.style[xProperty('transitionDuration')] = `${duration}ms`;
  }

  toggleSb(close?: boolean) {
    if (this.isTransiting) return;

    if (this.isSidebarOpen || close) {
      this.stepSb(0, ~~(this.sidebarWidth * 1.5));
      this.isSidebarOpen = false;
    }

    else {
      this.stepSb(this.sidebarWidth, ~~(this.sidebarWidth * 1.5));
      this.isSidebarOpen = true;
      this.isSearchOpen = false;
    }
  }

  toggleToc(close?: boolean) {
    if (this.toc.busy) return;
    if (this.isTocOpen || close) {
      this.toc.step(this.toc.width, ~~(this.toc.width * 1.5));
      this.isTocOpen = false;
    }

    else {
      this.toc.refresh();
      this.toc.syncPosition();
      this.toc.step(0, ~~(this.toc.width * 1.5));
      this.isTocOpen = true;
      this.isSearchOpen = false;
    }
  }

  toggleSearch(close?: boolean) {
    this.isSearchOpen = close ? false : !this.isSearchOpen;
  }

  setTitle(routeData: any) {
    // handle title
    const titleFn = ({
      post: data => data.post.title,
      page: data => data.page.title,
      tags: () => this.app.i18n('title.tags'),
      tag: data => this.app.i18n('title.tags') + ' : ' + data.tag.name,
      categories: () => this.app.i18n('title.categories'),
      category: data => this.app.i18n('title.categories') + ' : ' + data.category.name,
      archives: () => this.app.i18n('title.archives'),
      search: () => this.app.i18n('title.search'),
      404: () => 404,
    })[routeData.id];

    const title = titleFn ? titleFn(routeData) + ' - ' + this.config.title : this.config.title;
    this.title.setTitle(title);
  }

  private setColor(color?: string): string {
    color = color || this.app.config.color[1] || this.config.color[1] || '';
    this.meta.updateTag({ name: 'theme-color', content: color });
    return color;
  }

  onFabAct(action: FabAction) {
    switch (action) {
      case FabAction.toTop:
        this.animateTo(0);
        break;
      case FabAction.toBottom:
        this.animateTo(this.pageRef.nativeElement.scrollHeight - this.pageHeight);
        break;
      case FabAction.toggleSidebar:
        this.toggleSb();
        break;
      case FabAction.toggleToc:
        this.toggleToc();
        break;
      case FabAction.search:
        this.toggleSearch();
        break;
    }
  }

  animateTo(to: number) {
    const scrollTop = this.scrollTop || 0;
    const duration = Math.abs(~~((to - scrollTop) * .618 / 1));

    animate(this.pageRef.nativeElement, 'scrollTop', {
      from: scrollTop,
      to: to,
      duration: duration > 618 ? 618 : duration
    });
  }

  onOverlay() {
    if (this.isSidebarOpen) this.toggleSb();
    if (this.isTocOpen) this.toggleToc();
  }

  @HostListener('document:keyup', ['$event.key', '$event.which'])
  onkeyup(key: string, code: number) {
    if (key === 'Escape' || code === 27) {
      if (this.isSearchOpen) this.toggleSearch();
      if (this.isTocOpen) this.toggleToc();
      if (this.isSidebarOpen) this.toggleSb();
    }
  }
}
