import { Component } from '@angular/core';
import { Post, PostList } from '../../interface';
import { HPage } from '../shared';

@Component({
  selector: 'is-v-post-list',
  templateUrl: './v-post-list.component.html',
  host: { class: 'φv-post-list' },
})
export class VPostListComponent extends HPage<{ postList: PostList }> {
  assign(target: Post, merged: object): Post {
    return Object.assign({}, target, { pv: merged[target.link] });
  }
}
