import { Component, ElementRef, EventEmitter, Input, OnChanges, OnDestroy, OnInit, Output } from '@angular/core';
import { Subscription } from 'rxjs';
import { TocConfig } from '../../interface/config';
import { AppService, DeviceService } from '../../services';
import { transitionEnd, xProperty, debounce } from '../../utils';
import { g } from '../../global';

@Component({
  selector: 'is-toc',
  templateUrl: './toc.component.html',
  host: { class: 'φtoc' },
})
export class TocComponent implements OnInit, OnChanges, OnDestroy {
  @Input() toc;
  @Input() scrollTop = 0;
  @Output() action = new EventEmitter<number>();
  currentId = '';
  config: TocConfig;
  width: number;
  stacks: { [id: string]: { depth: string[], offset: number } } = {};
  busy: boolean;
  private linedIds: Array<{ id: string, offset?: number }> = [];
  private mediaSub: Subscription;

  constructor(
    private app: AppService,
    private device: DeviceService,
    public er: ElementRef
  ) {
    this.config = this.app.config.toc || {} as TocConfig;
    this.syncPosition = debounce(this.syncPosition.bind(this));
  }

  navigate(navigateId: string, e: MouseEvent) {
    e.preventDefault();
    if (this.currentId === navigateId) return;
    this.currentId = navigateId;

    const offset = this.getOffset(navigateId);
    if (offset >= 0) {
      this.action.emit(offset);
    }
  }

  // being called before or after open
  syncPosition() {
    const scrollTop = this.scrollTop || 0;

    if (scrollTop < this.linedIds[0].offset) {
      this.currentId = '';
      return;
    }

    for (let i = 0; i < this.linedIds.length; i++) {
      const item = this.linedIds[i], nextItem = this.linedIds[i + 1];
      if (this.currentId !== item.id
        && scrollTop >= item.offset
        && (nextItem === undefined || scrollTop < nextItem.offset)) {
        this.currentId = item.id;
        return;
      }
    }
  }

  private getOffset(id: string, hard?: boolean): number {
    const offset = this.stacks[id].offset;

    if (hard || offset === undefined) {
      const el = g.doc.getElementById(id);
      if (!el) return -1;
      return Math.floor(g.doc.getElementById(id).getBoundingClientRect().top) + (this.scrollTop || 0);
    }

    return offset;
  }

  ngOnInit() {
    this.mediaSub = this.device.media.subscribe(() => {
      this.refresh();
    });
  }

  ngOnChanges(changes) {
    if (changes.toc) setTimeout(() => this.updateStacks(), 100);
  }

  ngOnDestroy() {
    this.mediaSub.unsubscribe();
  }

  updateStacks() {
    this.stacks = test(this.toc);
    this.refresh();

    function test(toc, stacks = {}, _stack = []) {
      toc.forEach(({ id, children }) => {
        stacks[id] = { depth: _stack.concat(id) };

        if (children) test(children, stacks, stacks[id].depth);
      });
      return stacks;
    }
  }

  refresh() {
    for (const id in this.stacks) {
      this.stacks[id].offset = this.getOffset(id, true);
    }
    this.linedIds = Object.keys(this.stacks)
      .map(id => ({ id, offset: this.stacks[id].offset }))
      .sort((a: any, b: any) => a.offset - b.offset);
    this.width = this.er.nativeElement.offsetWidth;
  }

  step(translate: number, duration: number) {
    const toc = this.er.nativeElement;

    if (translate === this.width) {
      transitionEnd(toc, () => {
        toc.classList.remove('φtoc--active');
      });
    } else if (!toc.classList.contains('φtoc--active')) {
      toc.classList.add('φtoc--active');
    }

    toc.style[xProperty('transitionDuration')] = `${duration}ms`;
    toc.style[xProperty('transform')] = `translate3d(${translate}px, 0, 0)`;

    if (duration > 4) {
      this.busy = true;
      transitionEnd(toc, () => {
        this.busy = false;
      });
    }
  }
}
