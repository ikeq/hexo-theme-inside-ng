import { Component } from '@angular/core';
import { Page } from '../../interface';
import { HPage } from '../shared';

@Component({
  selector: 'is-v-page',
  templateUrl: './v-page.component.html',
  host: { class: 'φv-page' },
})
export class VPageComponent extends HPage<{ page: Page }> { }
