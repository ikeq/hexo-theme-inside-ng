import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';

interface PageItem {
  type?: 'prev' | 'next' | 'active' | 'dot';
  text?: number;
  route?: number;
}

@Component({
  selector: 'is-pager',
  templateUrl: './pager.component.html',
  host: { class: 'φpager' },
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PagerComponent implements OnInit, OnChanges {
  @Input() size: number = 0;
  @Input() count = 0;
  @Input() current = 0;
  @Input() url = '';
  @Input() indexUrl = '';
  @Output() page = new EventEmitter();
  items: PageItem[] = [];

  ngOnInit() {
    this.size = +this.size;
    this.count = +this.count;
    this.current = +this.current;

    if (!this.url) return;

    if (this.url[0] !== '/') this.url = '/' + this.url;
    if (this.indexUrl[0] !== '/') this.indexUrl = '/' + this.indexUrl;
  }

  ngOnChanges() {
    this.size = +this.size;
    this.count = +this.count;
    this.current = +this.current;

    this.makeItems();
  }

  private makeItems() {
    const out = [];
    // prev
    if (this.current > 1) out.push({ route: this.current - 1, type: 'prev' });
    // first
    if (this.current > 1) out.push({ route: 1, text: 1 });
    // ...
    if (this.current > 3) out.push({ type: 'dot' });
    if (this.current - 2 > 0) out.push({ route: this.current - 1, text: this.current - 1 });
    out.push({ text: this.current, type: 'active' });
    if (this.current < this.count - 1) out.push({ route: this.current + 1, text: this.current + 1 });
    // ...
    if (this.current < this.count - 2) out.push({ type: 'dot' });
    // last
    if (this.current < this.count) out.push({ route: this.count, text: this.count });
    // next
    if (this.current < this.count) out.push({ route: this.current + 1, type: 'next' });

    this.items = out as PageItem[];
  }

  link(page: number) {
    return page === 1 ? [this.indexUrl] : [this.url + '/' + page];
  }
}
