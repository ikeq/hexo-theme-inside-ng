import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'is-copyright',
  templateUrl: './copyright.component.html',
  host: { class: 'φcopyright' },
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CopyrightComponent {
  @Input() meta;
}
