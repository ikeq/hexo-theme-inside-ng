import { ChangeDetectionStrategy, Component, Input, OnChanges } from '@angular/core';

@Component({
  selector: 'is-img-loader',
  templateUrl: './img-loader.component.html',
  host: { class: 'φimg-loader' },
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImgLoaderComponent implements OnChanges {
  @Input() src: string;
  @Input() ratio = .625;
  @Input() alt?: string;

  state = -1; // -1 uninited, 0 error, 1 loaded

  width: number;

  ngOnChanges(changes) {
    if (changes.src) this.state = -1;
  }
  onLoad({ target }: any) {
    this.ratio = target.height / target.width;
    this.width = target.width;
    this.state = 1;
  }
  onError() {
    this.state = 0;
  }
}
