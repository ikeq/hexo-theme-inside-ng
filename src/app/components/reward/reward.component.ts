import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { PaymentMethod } from '../../interface';
import { AppService } from '.././../services';
import { debounce } from '../../utils';

@Component({
  selector: 'is-reward',
  templateUrl: './reward.component.html',
  host: { class: 'φreward' },
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RewardComponent implements OnInit, OnDestroy {
  lock: boolean;
  busy: boolean;
  text: string;
  methods: PaymentMethod[];
  select: PaymentMethod = {};

  private routerSub: Subscription;

  constructor(
    private cd: ChangeDetectorRef,
    private router: Router,
    app: AppService
  ) {
    this.text = app.config.reward.text;
    this.methods = app.config.reward.methods.map((method, index) => {
      if (~['wechat', 'paypal', 'bitcoin'].indexOf(method.name)) method.icon = method.name;
      method.index = index;
      return method;
    });

    this.onClick = debounce(this.onClick.bind(this), 100);
  }

  ngOnInit() {
    this.routerSub = this.router.events.subscribe(() => {
      this.select = {};
      this.cd.detectChanges();
    });
  }

  ngOnDestroy() {
    this.routerSub.unsubscribe();
  }

  onClick(method: PaymentMethod) {
    if (this.busy) return;
    if (this.select.index === method.index) {
      this.select = {};
    } else {
      this.select = method;
    }
    this.cd.detectChanges();
  }
}
