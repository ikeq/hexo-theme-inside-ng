import { Component, ElementRef, EventEmitter, HostBinding, Input, OnInit, Output, ViewChild } from '@angular/core';
import { AppService } from '../../services';
import { debounce } from '../../utils';
import { SearchList, SearchService } from './search.service';

@Component({
  selector: 'is-v-search',
  templateUrl: './v-search.component.html',
  host: { class: 'φv-search' },
})
export class VSearchComponent implements OnInit {
  keyword: string;
  config: any;
  res: SearchList;
  tiMap: any;
  @ViewChild('input', { static: true }) private inputRef: ElementRef;
  @HostBinding('class.' + 'φv-search--fixed') @Input() fixed: string;
  @Output() exit = new EventEmitter<void>();

  constructor(private search: SearchService, app: AppService) {
    this.onSearch = debounce(this.onSearch.bind(this), 500);
    this.config = app.config.search;
  }

  onSearch(value: string) {
    this.keyword = value.trim();
    this.res = null;
    if (this.keyword) this.onQuery();
  }
  onQuery(current?: number) {
    this.search.query(this.keyword, current)
      .then(data => {
        this.res = data;
        this.tiMap = { time: data.time, hits: data.hits, query: this.keyword };
      });
  }
  ngOnInit() {
    if (this.fixed) this.inputRef.nativeElement.focus();
  }
}
