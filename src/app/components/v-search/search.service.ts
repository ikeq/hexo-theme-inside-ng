import { Injectable } from '@angular/core';
import { HttpService, AppService } from '../../services';
import { SearchConfig, Post, PostList } from '../../interface';
import { getData } from '../../utils';

export interface SearchList extends PostList {
  hits: number;
  time: number;
}

class Search {
  keyword: string;
  config: SearchConfig;
  http: HttpService;
  readonly perPage: number;
  constructor(config: SearchConfig, http: HttpService) {
    this.config = config;
    this.http = http;
    this.perPage = config.per_page;
  }
}

class CustomSearch extends Search {
  private cache: { [query: string]: { [index: string]: SearchList } } = {};
  pageOffset: number;
  constructor(config: SearchConfig, http: HttpService) {
    super(config, http);
  }
  query(query: string, current: number): Promise<SearchList> {
    if (this.cache[query] && this.cache[query][current || 1]) return Promise.resolve(this.cache[query][current || 1]);

    const request = { ...this.config.request };
    // body template interpolation
    request.body = (<any>request.body)
      .replace(/:query/g, query)
      .replace(/:per_page/g, this.perPage + '')
      // request need to restore to the original page index
      .replace(/:current/g, current === undefined ? '' : (current - this.pageOffset));

    // Request with GET/HEAD method cannot have body.
    if (!request.method || request.method.toLowerCase() === 'get') {
      if (request.url.split('?')[1]) request.url += '&' + request.body;
      else request.url += '?' + request.body;
      delete request.body;
    }

    const now = Date.now();
    return this.http.request(request)
      .then(res => {
        const { keys } = this.config;

        let data = getData(res, keys.data);
        let current = getData(res, keys.current);
        let total = getData(res, keys.total);
        let hits = getData(res, keys.hits);
        let time = getData(res, keys.time);

        // simulate cost time
        if (time === undefined)
          time = Date.now() - now;

        if (!data || !Array.isArray(data)) {
          data = [];
          hits = 0;
        }

        if (typeof hits !== 'number' || !hits)
          hits = data.length;

        if (typeof current !== 'number')
          current = 1;
        else if (this.pageOffset === undefined)
          // current=0 means page start from index 0, we store pageOffset as 1
          // current=1 means page start from index 1, we store pageOffset as 0
          this.pageOffset = current ^ 1;

        // ensure total=1 if data is not empty
        if (typeof total !== 'number')
          total = data.length ? (hits ? Math.ceil(hits / this.perPage) : 1) : 0;

        // title and content replacement
        if (keys.title || keys.content) {
          data = data.map(hit => {
            const title = getData(hit, keys.title);
            const content = getData(hit, keys.content);

            if (title) hit.title = title;
            if (content) hit.content = content;

            return hit;
          });
        }

        const ret: any = {
          // returnd page index must start from 1
          current: current + this.pageOffset,
          data, hits, time, total
        };

        // cache
        (this.cache[query] || (this.cache[query] = {}))[ret.current] = ret;

        return ret;
      });
  }
}

@Injectable({
  providedIn: 'root',
  useFactory: (http: HttpService, app: AppService) => {
    const config = app.config.search;
    if (config.local) return new SearchService(config, http);
    else return new CustomSearch(config, http);
  },
  deps: [HttpService, AppService]
})
export class SearchService extends Search {
  entries: Post[];
  hits: Post[];
  total: number;
  time: number;

  query(query: string, current: number): Promise<SearchList> {
    return (!this.entries ? this.http.get('search').then(data => this.entries = data) : Promise.resolve(this.entries))
      .then(data => {
        // previous query
        if (this.keyword && query === this.keyword) {
          return this.page(current);
        }

        const hits = [];
        const now = Date.now();

        data.forEach(post => {
          const matchedTitle = this.match(post.title, query);
          const matchedContent = this.match(post.content, query);

          if (matchedTitle || matchedContent) hits.push({
            ...post,
            title: matchedTitle || post.title,
            content: matchedContent
          });
        });

        this.keyword = query;
        this.hits = hits;
        this.total = Math.ceil(this.hits.length / this.perPage);
        this.time = Date.now() - now;

        return this.page();
      });
  }

  page(current: number = 1): SearchList {
    return {
      current,
      total: this.total,
      per_page: this.perPage,
      hits: this.hits.length,
      time: this.time,
      data: this.hits.slice(this.perPage * (current - 1), this.perPage * current)
    };
  }

  match(text: string, str: string): void | string {
    if (!str) return;

    const lowerCasedText = text.toLowerCase();
    const keywords = str.toLowerCase().split(' ');
    const hitTests = keywords.map(w => lowerCasedText.indexOf(w));

    if (hitTests.filter(i => !~i).length) return;

    const hits = [];
    const firstHit = hitTests.sort()[0];

    const start = text.length > 100 && firstHit > 20 ? firstHit - 20 : 0;
    const end = start + 100;

    text = text.substring(start, end);

    lowerCasedText.substring(start, end)
      .replace(new RegExp(keywords.join('|'), 'g'), (matched, index) => {
        hits.push([index, matched.length]);
        return '';
      });

    let ret = '';
    let before = 0;
    hits.forEach(([index, offset]) => {
      ret += text.substring(before, index);
      before = index + offset;
      ret += `<em>${text.substring(index, before)}</em>`;
    });
    ret += text.substring(before, 100);

    return ret;
  }
}
