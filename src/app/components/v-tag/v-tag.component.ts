import { Component } from '@angular/core';
import { Tags } from '../../interface';
import { HPage } from '../shared';

@Component({
  selector: 'is-v-tag',
  templateUrl: './v-tag.component.html',
  host: { class: 'φv-tag' },
})
export class VTagComponent extends HPage<{ tag: Tags }> { }
