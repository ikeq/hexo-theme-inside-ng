import { ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { AppService } from '../../services';

export interface CommentMeta {
  title: string;
  url: string;
  id: string;
  thumbnail?: string;
}

@Component({
  selector: 'is-comment',
  templateUrl: './comment.component.html',
  host: { class: 'φcomment' },
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CommentComponent implements OnChanges {
  plugins: string[];

  @Input() page: CommentMeta;

  constructor(
    private app: AppService,
  ) {
    this.plugins = this.app.getPlugins('comments');
  }

  ngOnChanges({ page: { currentValue: page } }: SimpleChanges) {
    if (this.app.config.ssr) return;
    document.dispatchEvent(new CustomEvent('inside', {
      detail: {
        type: 'route',
        data: { ...page }
      }
    }));
  }
}
