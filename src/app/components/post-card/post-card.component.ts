import { Component, Input, ChangeDetectionStrategy } from '@angular/core';
import { Post } from '../../interface';

@Component({
  selector: 'is-post-card',
  templateUrl: './post-card.component.html',
  host: { class: 'φpost-card' },
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PostCardComponent {
  @Input() post: Post;
}
