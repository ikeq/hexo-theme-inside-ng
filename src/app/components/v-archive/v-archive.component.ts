import { Component } from '@angular/core';
import { Archives } from '../../interface';
import { HPage } from '../shared';

@Component({
  selector: 'is-v-archive',
  templateUrl: './v-archive.component.html',
  host: { class: 'φv-archive' },
})

export class VArchiveComponent extends HPage<{ archives: Archives }> { }
