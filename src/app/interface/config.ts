import { Appearance } from './appearance';

export interface PaymentMethod {
  name?: string;
  index?: number;
  icon?: string;
  qrcode?: string;
  url?: string;
  text?: string;
  color?: string;
}

export interface TocConfig {
  depth?: number;
  index?: boolean;
}

export interface CopyrightConfig {
  author?: string;
  link?: string;
  license: string;
  published?: string;
  updated?: string;
  custom?: string;
}

export interface PluginsConfig {
  $t: string[];
  sidebar?: number[];
  page?: number[];
  post?: number[];
  comments?: number[];
}

export interface SearchConfig {
  /**
   * render /search
   */
  page?: true;
  /**
   * display fab button
   */
  fab?: true;

  /**
   * Local search adapter
   */
  local?: true;
  per_page: number;

  /**
   * Custom search adapter
   */
  logo?: string;
  request?: Request;
  keys?: {
    data: string;
    current: string;
    total?: string;
    hits?: string;
    time?: string;
    title?: string;
    content?: string;
  };
}

export interface Config {
  ssr?: boolean;
  title: string;
  author: string;
  url: string;
  plugins?: PluginsConfig;
  profile: {
    email: string;
    bio: string;
    avatar?: string;
  };
  menu?: [string, string, 1?];
  sns?: Array<[string, string, string?]>;
  routes?: { posts: string[], pages?: string[] };
  footer: {
    copyright?: string;
    powered?: string;
    theme?: string;
    custom?: string;
  };
  toc?: TocConfig;
  reward?: {
    text: string;
    methods: PaymentMethod[];
  };
  count: {
    posts: number;
    categories: number;
    tags: number;
  };
  category0?: string;
  /**
   * either `page` (stands for post list)
   * or `index` (stands for any post or page)
   */
  index: 'page' | 'index';
  data_prefix: string;
  hash: string;
  locale: { [name: string]: string; };
  /** [string, string?] */
  color: string[];
  extend: {
    theme: {
      name: string;
      url: string;
    };
    background: string;
  };
  search?: SearchConfig;
  theme: {
    default: Appearance;
    dark?: Appearance;
  };
}
