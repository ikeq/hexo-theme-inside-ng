export enum ScreenType { 'sm', 'md', 'lg' }
export interface Screen {
  type: ScreenType;
}
