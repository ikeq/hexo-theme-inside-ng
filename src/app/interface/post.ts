import { PageData, DateFormatted } from './common';
import { Tag } from './tag';
import { Category } from './category';
import { CopyrightConfig } from './config';

export interface Post {
  title: string;
  date: string;
  date_formatted: DateFormatted;
  reading_time?: string;
  pv?: string;
  updated: string;
  excerpt: string;
  content: string;
  color?: string;
  thumbnail?: string;
  dropcap: boolean;
  categories: Category[];
  tags: Tag[];
  link: string;
  plink: string;
  toc?: boolean;
  comments?: boolean;
  reward?: boolean;
  copyright?: CopyrightConfig;
  prev?: {
    link: string;
    title: string;
  };
  next?: {
    link: string;
    title: string;
  };
  custom?: string;
}
export interface PostList extends PageData {
  data: Post[];
}
