import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'assign'
})
export class AssignPipe implements PipeTransform {
  transform(target: object, merged: object): object {
    return Object.assign(target, merged);
  }
}
