import { Pipe, PipeTransform } from '@angular/core';
import { AppService } from '../services';

@Pipe({
  name: 'i18n'
})
export class I18nPipe implements PipeTransform {
  constructor(private app: AppService) { }

  transform(key: string, ...args): string {
    return this.app.i18n(key, ...args);
  }
}
