import { Injectable, Inject } from '@angular/core';
import { AppService } from './app.service';
import { LoaderService } from './loader.service';
import { base64 } from '../utils';
import { FETCH } from '../tokens';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  prefix: string;
  version = '';

  constructor(
    private loader: LoaderService,
    private app: AppService,
    @Inject(FETCH) private fetch: any
  ) {
    this.prefix = this.app.config.data_prefix;
    this.version = this.app.config.hash || '';
  }

  get(url: string): Promise<any> {
    url = this.getFullUrl(url);

    const cache = this.app.getCache(url.split('?')[0]);

    if (cache) {
      return Promise.resolve(cache);
    }

    this.loader.show();
    // TODO:
    return this.fetch(url)
      .then(res => {
        this.loader.hide();
        return res;
      })
      .catch(e => {
        console.error(e);
        this.loader.hide();
        return Promise.reject(e);
      });
  }

  request({ url, ...config}: Request) {
    this.loader.show();
    return this.fetch(url, config)
      .then(res => {
        this.loader.hide();
        return res;
      })
      .catch(e => {
        this.loader.hide();
        return Promise.reject(e);
      });
  }

  private getFullUrl(url: string): string {
    return `${this.prefix}/${base64(url.replace(/(^\/*|\/*$)/g, ''))}.json?v=${this.version}`;
  }
}
