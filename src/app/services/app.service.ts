import { DOCUMENT } from '@angular/common';
import { Inject, Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Config, PluginsConfig } from '../interface';
import { CONFIG } from '../tokens';
import { sprintf } from '../utils';
import { g } from '../global';
import { InsideEvent } from '../interface/common';

@Injectable({
  providedIn: 'root',
})
export class AppService {
  private eventSubject = new Subject();
  private locales: { [key: string]: { plural?: boolean; value: string } };

  root: HTMLElement;
  config: Config;
  event = this.eventSubject.asObservable();

  constructor(
    @Inject(DOCUMENT) private doc: Document,
    @Inject(CONFIG) getConfig: () => Config
  ) {
    this.config = getConfig();
    const { locale } = this.config;
    const data = {};

    /**
     * {
     *   key1: {
     *     value: ''
     *   },
     *   key2: {
     *     plural: true,
     *     value: ['=0', '=1,' 'other']
     *   }
     * }
     */
    const pluralMap = { zero: 0, one: 1, other: 2 };
    for (const key in locale) {
      const match = key.match(/\.(zero|one|other)$/);
      if (match) {
        const k = key.substring(0, match.index);
        if (!data[k]) data[k] = { plural: true, value: [] };

        data[k].value[pluralMap[match[1]]] = locale[key];
      } else {
        data[key] = { value: locale[key] };
      }
    }
    this.locales = data;

    g.doc.addEventListener('inside', (event: CustomEvent<InsideEvent>) => {
      this.eventSubject.next(event.detail);
    });
  }

  /**
   * i18n('title.tags')                    => Tag
   * i18n('comments.load', 'Disqus')       => Load Disqus
   * i18n('page.tags', 0)                  => No tags
   * i18n('page.tags', 1)                  => 1 tag in total
   * i18n('page.tags', 2)                  => 1 tags in total
   * i18n('search.hits', 0, {query: 'hi'}) => No result found for "hi"
   *
   * @param key              i18n key
   * @param listOrMapOrCount count for plural | [%s, %s] | {':query': 'keyword}
   * @param listOrMap        [%s, %s] | {':query': 'keyword}
   */
  i18n(
    key: string,
    listOrMapOrCount?: (string | number)[] | { [key: string]: string | number } | number,
    listOrMap?: (string | number)[] | { [key: string]: string | number }
  ): string {
    const locale = this.locales[key];
    if (!locale || !locale.value) return key;
    if (locale.plural) {
      // listOrMapOrCount is number
      const template = locale.value[Math.min(<number>listOrMapOrCount || 0, locale.value.length - 1)];
      if (!template) return key;
      return sprintf(sprintf(template, listOrMapOrCount), listOrMap);
    }

    return sprintf(locale.value, listOrMapOrCount as any);
  }

  getPlugins(position: string): string[] {
    const plugins = this.config.plugins || {} as PluginsConfig;
    return plugins[position] ? plugins[position].map(index => plugins.$t[index]).filter(i => i) : null;
  }

  // init(): void { }

  getCache(id: string) {
    const script = this.doc.querySelector('[is="state"]');
    let data = {};
    if (script && script.textContent) {
      try {
        data = JSON.parse(this.unescapeHtml(script.textContent));
      } catch (e) {
        console.warn('Exception occured while parsing state', e);
      }

      return data[id];
    }

    return null;
  }

  /**
   * @param {?} text
   * @return {?}
   */
  private unescapeHtml(text) {
    const unescapedText = {
      '&a;': '&',
      '&q;': '"',
      '&s;': '\'',
      '&l;': '<',
      '&g;': '>',
    };
    return text.replace(/&[^;]+;/g, s => unescapedText[s]);
  }
}
