import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { AppService } from './app.service';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root',
})
export class DataResolver<T> implements Resolve<T> {
  private track = 0;

  constructor(private http: HttpService, private router: Router, private app: AppService) { }

  resolve(route: ActivatedRouteSnapshot): Promise<T> {
    const url = route.pathFromRoot.reduce<string[]>((paths, current): string[] => {
      current.url.forEach(i => paths.push(i.path));
      return paths;
    }, []).join('/');

    return this.http.get(url.replace(/\/1$/, '') || this.app.config.index)
      .catch(this.error.bind(this));
  }

  private error(e: Error) {
    // TODO: toast
    if (this.track < 1) {
      this.router.navigate(['/']);
      this.track++;
    }
    return {};
  }
}
