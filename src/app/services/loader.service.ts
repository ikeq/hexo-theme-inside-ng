import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LoaderService {
  private subject = new Subject<boolean>();
  state = this.subject.asObservable();
  busy = false;

  constructor() { }

  show() {
    this.busy = true;
    this.subject.next(true);
  }
  hide() {
    this.busy = false;
    this.subject.next(false);
  }
}
