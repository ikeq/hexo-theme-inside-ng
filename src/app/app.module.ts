import { NgModule, APP_INITIALIZER } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import {
  AppComponent,
  ArchiveComponent,
  CategoryComponent,
  CommentComponent,
  FabComponent,
  PagerComponent,
  PostCardComponent,
  SidebarComponent,
  TagComponent,
  TocComponent,
  CopyrightComponent,
  RewardComponent,
  ImgLoaderComponent,
  VArchiveComponent,
  VCategoryComponent,
  V404Component,
  VPageComponent,
  VPostComponent,
  VPostListComponent,
  VTagComponent,
  VTagListComponent,
  VSearchComponent
} from './components';
import { UnsafePipe, I18nPipe, AssignPipe } from './pipes';
import { AppService, DeviceService, HttpService, LoaderService } from './services';
import { SnippetDirective, ZoomableDirective } from './directives';

@NgModule({
  declarations: [
    UnsafePipe,
    I18nPipe,
    AssignPipe,

    SnippetDirective,
    ZoomableDirective,

    AppComponent,
    VPageComponent,
    VArchiveComponent,
    V404Component,
    VPostListComponent,
    VPostComponent,
    VTagListComponent,
    VTagComponent,
    VCategoryComponent,
    VSearchComponent,

    ArchiveComponent,
    SidebarComponent,
    FabComponent,
    PagerComponent,
    TagComponent,
    TocComponent,
    CategoryComponent,
    CommentComponent,
    PostCardComponent,
    CopyrightComponent,
    RewardComponent,
    ImgLoaderComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'inside' }),
    AppRoutingModule
  ],
  providers: [
    // {
    //   provide: APP_INITIALIZER,
    //   useFactory: (app: AppService) => () => app.init(),
    //   deps: [AppService],
    //   multi: true
    // },
    HttpService,
    AppService,
    DeviceService,
    LoaderService,

    I18nPipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
