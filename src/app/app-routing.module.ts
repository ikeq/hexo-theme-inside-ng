import { NgModule, Injectable } from '@angular/core';
import { RouterModule, Routes, RouteReuseStrategy, ActivatedRouteSnapshot, DetachedRouteHandle } from '@angular/router';
import {
  CategoryComponent,
  VArchiveComponent,
  VCategoryComponent,
  V404Component,
  VPostListComponent,
  VTagComponent,
  VTagListComponent,
  VSearchComponent
} from './components';
import { DataResolver } from './services';

@Injectable()
export class BypassPost implements RouteReuseStrategy {
  shouldDetach() { return false; }
  store(): void { }
  shouldAttach() { return false; }
  retrieve() { return null; }
  shouldReuseRoute(future: ActivatedRouteSnapshot, curr: ActivatedRouteSnapshot): boolean {
    // bypass post and page
    if (
      future.routeConfig &&
      future.routeConfig.data &&
      ~['post', 'page'].indexOf(future.routeConfig.data.id) &&
      String(future) !== String(curr)
    ) return false;
    return future.routeConfig === curr.routeConfig;
  }
}

/**
 * All built-in routes are safe, including:
 * page, tags, categories, archives, 404, search
 */
export const routes: Routes = [
  // post list
  { path: 'page/:page', component: VPostListComponent, resolve: { postList: DataResolver }, data: { id: 'posts' } },

  /* page & post routes will be loaded later */

  // search
  { path: 'search', component: VSearchComponent, data: { id: 'search' } },

  // tag
  { path: 'tags', component: VTagListComponent, resolve: { tagList: DataResolver }, data: { id: 'tags' } },
  { path: 'tags/:name', component: VTagComponent, resolve: { tag: DataResolver }, data: { id: 'tag' } },
  { path: 'tags/:name/:page', component: VTagComponent, resolve: { tag: DataResolver }, data: { id: 'tag' } },

  // category
  {
    path: 'categories',
    component: VCategoryComponent,
    resolve: { categoryList: DataResolver },
    data: { id: 'categories' },
    children: [
      { path: ':name', component: CategoryComponent, resolve: { category: DataResolver }, data: { id: 'category' } },
      { path: ':name/:page', component: CategoryComponent, resolve: { category: DataResolver }, data: { id: 'category' } }
    ]
  },

  // archives
  { path: 'archives', component: VArchiveComponent, resolve: { archives: DataResolver }, data: { id: 'archives' } },
  { path: 'archives/:page', component: VArchiveComponent, resolve: { archives: DataResolver }, data: { id: 'archives' } },

  { path: '404', component: V404Component, data: { id: '404' } }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {})],
  exports: [RouterModule],
  providers: [
    {
      provide: RouteReuseStrategy,
      useClass: BypassPost
    },
    DataResolver
  ]
})
export class AppRoutingModule { }
