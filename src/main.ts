import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { CONFIG, FETCH } from './app/tokens';

enableProdMode();

platformBrowserDynamic([
  { provide: CONFIG, useValue: () => window.__inside__ },
  { provide: FETCH, useValue: (url: string, options: RequestInit | undefined) => fetch(url, options).then(r => r.json()) }
]).bootstrapModule(AppModule)
  .catch(() => void 0);
