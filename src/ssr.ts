import 'zone.js/node';
import { CommonEngine, RenderOptions as NgRenderOptions } from '@nguniversal/common/engine';
import { CONFIG, FETCH } from './app/tokens';
import { AppServerModule } from './main.server';

export { CONFIG, FETCH };

interface RenderOptions {
  resolve: {
    config: any;
    data: (url: string) => any;
  };
  document: string;
  url: string;
}

/**
 * @example
 * import { Renderer, CONFIG, FETCH } from './ssr';
 *
 * const render = Renderer();
 * render({
 *   resolver: {
 *     config: {},
 *     fetch: () => {}
 *   },
 *   document: 'html',
 *   url: '/about'
 * });
 */
export function Renderer() {
  const engine = new CommonEngine();

  return (options: RenderOptions) => {
    // Cache current fetched json to be injected into html lately
    const stateInjector = (() => {
      const state: { [url: string]: object } = {};
      return {
        add(key: string, value: any) {
          state[key] = value;
        },
        inject(html: string) {
          if (!state) return html;
          return html.replace('</head>', `<script type="application/json" is="state">${escapeHtml(JSON.stringify(state))}</script></head>`);
        }
      };

      function escapeHtml(text) {
        /** @type {?} */
        const escapedText = {
          '&': '&a;',
          '"': '&q;',
          '\'': '&s;',
          '<': '&l;',
          '>': '&g;',
        };
        return text.replace(/[&"'<>]/g, s => escapedText[s]);
      }
    })();
    const renderOptions: NgRenderOptions = {
      document: options.document,
      url: options.url,
      bootstrap: AppServerModule,
      inlineCriticalCss: false,
      providers: [
        { provide: CONFIG, useValue: () => options.resolve.config },
        {
          provide: FETCH,
          useValue: (apiUrl: string): Promise<any> => {
            apiUrl = apiUrl.split('?')[0];
            const json = options.resolve.data(apiUrl);
            stateInjector.add(apiUrl, json);

            return Promise.resolve(json);
          }
        },
      ]
    };

    return engine.render(renderOptions).then(stateInjector.inject);
  };
}
