import { enableProdMode } from '@angular/core';

enableProdMode();

export { AppServerModule } from './app/app.server.module';
export { renderModuleFactory } from '@angular/platform-server';
