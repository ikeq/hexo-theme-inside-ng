import path from 'path';
import typescript from '@rollup/plugin-typescript';
import { terser } from 'rollup-plugin-terser';
import replace from '@rollup/plugin-replace';

const production = !process.env.ROLLUP_WATCH;
const root = path.join(__dirname, '../');
const outDir = path.join(root, 'dist/tmp/browser');

export default {
  input: path.join(root, 'src/app/styles/theme.ts'),
  output: {
    sourcemap: !production,
    format: 'umd',
    entryFileNames: 'theme.[hash].js',
    dir: outDir,
    name: '__inside__',
  },
  plugins: [
    typescript({
      lib: ['es2018', 'dom'],
      target: 'es6',
      rootDir: 'src',
      outDir,
      sourceMap: false,
    }),
    replace({
      'process.env.NODE_ENV': JSON.stringify(production && 'production')
    }),
    production && terser({ format: { comments: false } }),
    {
      generateBundle(options, bundle) {
        const themeFileName = Object.keys(bundle)[0];
        const { code } = bundle[themeFileName];
        bundle[themeFileName].code = code.replace('__inside__={}', '__inside__||(self.__inside__={})');
      },
    },
  ],
};
