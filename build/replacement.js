const fs = require('fs-extra');
const path = require('path');
const getId = require('./utils').alphabetId();

// save selector map
const components = {};

const r = {
  φ(ignoreMissing) {
    return content => content.replace(/(φ[\w_-]+)/g, (_, $1) => {
      if (!css[$1]) {
        if (ignoreMissing) return ''
        else throw 'Unknown name:' + $1
      }
      return css[$1]
    })
  }
};



['src', 'angular.json', 'tsconfig.json', 'tsconfig.app.json', 'tsconfig.server.json']
  .forEach(name => {
    fs.copySync(path.join(__dirname, '..', name), path.join(__dirname, '../dist', name));
  });

const src = path.join(__dirname, '../dist/src');
const temp = path.join(__dirname, '../dist/tmp');
const css = require(path.join(temp, 'css.js'));

// components
fs.readdirSync(path.join(src, 'app/components'))
  .filter(i => !i.endsWith('.ts')).sort()
  .map(cName => {
    replaceContent(
      `app/components/${cName}/${cName}.component.ts`,
      content => {
        return r.φ(true)(content)
          // replacement for selector: '',
          .replace(/(?<=selector: *['"])([\w-]+)/, (_, $1) => {
            return components[$1] = 'is-' + getId($1)
          })
      }
    );
    return cName
  })
  .map(cName => {
    replaceContent(
      `app/components/${cName}/${cName}.component.html`,
      content => {
        return r.φ()(content)
          // replacement for <is-name>,
          .replace(/(?<=\<\/?)(is[\w-]+)/g, (_, $1) => {
            return 'is-' + getId($1)
          })
      }
    );
    return cName
  });

fs.writeJSONSync(path.join(temp, 'components.json'), components);

// process directives
fs.readdirSync(path.join(src, 'app/directives'))
  .filter(i => i.endsWith('.directive.ts'))
  .forEach(filename => {
    replaceContent(`app/directives/${filename}`, r.φ());
  });

function replaceContent(filename, replacer) {
  const distPath = path.join(src, filename);
  const content = fs.readFileSync(distPath, 'utf8');

  fs.ensureDirSync(path.dirname(distPath));
  fs.writeFileSync(path.join(distPath), replacer(content));
}
