import path from 'path';
import postcss0 from 'postcss';
import postcss from 'rollup-plugin-postcss'
import replace from '@rollup/plugin-replace';
import url from "postcss-url"

const root = path.join(__dirname, '../');
const production = !process.env.ROLLUP_WATCH;

/** @type {[RegExp, string]} */
const themeVars = require('./utils').parseTs('app/styles/theme.ts').css({}, true)
  .map(i => [new RegExp(`(?<!_)${i[0]}(?!_)`), i[1]]);

// create cssMap (for production only)
const mapMode = process.env.npm_lifecycle_event === 'build:before';
const css = mapMode ? {} : require(path.join(root, 'dist/tmp/css'))

export default {
  input: mapMode ? {
    css: path.join(root, 'src/app/styles/index.js')
  } : path.join(root, 'src/app/styles/index.scss'),
  output: {
    format: 'commonjs',
    dir: 'dist/tmp',
    sourcemap: false
  },
  plugins: [
    postcss({
      extract: './public/styles.css',
      plugins: [
        url({
          url: 'inline',
          maxSize: 100,
          filter: /\.(woff2?|svg)$/
        }),
        postcss0.plugin('postcss-replace', () => css => {
          css.walk(node => {
            if (node.constructor.name !== 'Declaration') return;
            const match = themeVars.find(i => i[0].test(node.value));
            if (match) {
              node.value = node.value.replace(match[0], match[1]);
            }
          })
        })
      ].concat(production ? [
        require('autoprefixer'),
        // temporarily disable due to https://github.com/cssnano/cssnano/issues/701
        require('cssnano')({ preset: ['default', { mergeRules: false }] })
      ] : []),
      use: [
        ['sass', {
          includePaths: [path.join(root, 'src/app/styles')],
        }]
      ],
      modules: {
        scopeBehaviour: 'local',
        generateScopedName(name) {
          if (!production || mapMode) return name
          if (!css[name]) throw 'Unknown name:' + name
          return css[name]
        }
      },
      minimize: production
        ? {}
        : false
    }),
    replace({
      'process.env.NODE_ENV': JSON.stringify(production && 'production')
    }),
  ]
}
